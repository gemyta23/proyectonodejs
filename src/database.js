const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/proyectoNodejs_curso',{
    useNewUrlParser: true
})

.then(db => console.log('DB is connected'))
.catch(err => console.err(err));
