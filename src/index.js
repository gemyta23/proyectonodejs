const express = require('express');
const path = require('path');
const morgan = require('morgan');
const multer = require('multer');
const uuid = require('uuid');
const {format} = require('timeago.js');

//inicializaciones 
const app = express();
require('./database');

//voy a listar las configuraciones de mi sevidor (los setting)

app.set('port', process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs')

//midelwares para el servidor

app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public/image/uploads'),
    filename:(req, file, cb, filename) => {
        cb(null,uuid.v4() + path.extname(file.originalname));

    }
});
app.use(multer({storage: storage}).single('image'));




//variables globales

//app.use((req, res, next)=> {
    //app.locals.format = format;
    //next();
//});
//<p class="card-text"><%= format(image.created_at) %></p> (index.ejs)



//routes (rutas)

app.use(require('./routes/index'));

//archivos estáticos

app.use(express.static(path.join(__dirname, 'public')));

//inicar servidores

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});